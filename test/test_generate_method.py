from pathlib import Path
from unittest.mock import patch, MagicMock

from pip_compile_cross_platform import generate


def test_generate_hashes():
    workd_dir = MagicMock(Path)
    with patch("pip_compile_cross_platform._run_poetry") as mock_run_poetry:
        with patch("pip_compile_cross_platform.create_poetry_project"):
            generate(
                workd_dir,
                requirements=[],
                existing_requirements=[],
                min_python_version="",
                generate_hashes=True,
                output_file=MagicMock(Path),
                input_command=[],
            )
            mock_run_poetry.assert_any_call(workd_dir, "export")


def test_not_generate_hashes():
    workd_dir = MagicMock(Path)
    with patch("pip_compile_cross_platform._run_poetry") as mock_run_poetry:
        with patch("pip_compile_cross_platform.create_poetry_project"):
            generate(
                workd_dir,
                requirements=[],
                existing_requirements=[],
                min_python_version="",
                generate_hashes=False,
                output_file=MagicMock(Path),
                input_command=[],
            )
            mock_run_poetry.assert_any_call(workd_dir, "export", "--without-hashes")
