#!python3

import glob
import subprocess
import sys
from pathlib import Path
from subprocess import CalledProcessError
from typing import Union


def run_tool(*args: Union[str, Path]):
    subprocess.run([sys.executable, "-m", *args], check=True)


def lint():
    project_root = Path(__file__).parent
    python_files = glob.glob("**/*.py", recursive=True)

    run_tool(
        "mypy",
        "--no-error-summary",
        project_root,
    )

    run_tool(
        "pyupgrade",
        "--exit-zero-even-if-changed",
        "--py37-plus",
        *python_files,
    )

    run_tool(
        "black",
        "--quiet",
        project_root,
    )


if __name__ == "__main__":
    try:
        lint()
    except CalledProcessError:
        sys.exit(1)
    print("✅ Lint successful")
