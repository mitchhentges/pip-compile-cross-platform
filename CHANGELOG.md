# 1.4.2

* [Add `--generate-hashes` argument](https://gitlab.com/mitchhentges/pip-compile-cross-platform/-/merge_requests/19) (thanks @jgalda!)

# 1.4.1

* [Fix `--output-file` argument](https://gitlab.com/mitchhentges/pip-compile-cross-platform/-/merge_requests/18) (thanks @jgalda!)

# 1.4.0

* Use [`poetry`'s new "non-package mode"](https://github.com/python-poetry/poetry/pull/8650)

# 1.3.0

* Bump `poetry` version to `^1.8.1`, resolving the "poetry-plugin-export" warning.
* Tweak how requirement lines are fed to Poetry - this should mean that remote URLs are now supported.
* Bump default `--min-python-version` to `3.8`

# 1.2.0

* Don't drop environment markers from source `requirements.in` file 

# 1.1.0

* Bump `poetry` to `1.3.1`

# 1.0.1

* Pin `poetry-core` to avoid "empty requirements.txt" issue

# 1.0.0

* Updated README to better clarify purpose of `pip-compile-cross-platform`
* Added automated integration tests to ensure stability

# 0.9.3

* Add support for `setup.py` input files
* Add support for `requirements.in`-format input files that link to other projects on the filesystem
* Improve output to be less noisy and more descriptive.

# 0.9.2

Fix "No module named 'tomli'" error

# 0.9.1

Support reusing existing lockfile, causing minimal changes

# 0.9.0

Initial release
